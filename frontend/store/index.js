import Vue from 'vue';
import Vuex from 'vuex';
import { gPostsCtrl as ctrl } from '../ctrl_data/ctrl_data';
import { fGetIx, fPaginate } from '../ctrl_data/service/FormatS';

Vue.use(Vuex);

const store = () => new Vuex.Store({
  state: {
	/** Полный список постов */
	post_list_full: [],

	/** Интервал дат для поиска */
	date_interval: [],

	/** Список постов для вывода за один раз */
	post_list: [],

	/** Индексированный список постов */
	ix_post_list_full: {},

	/** Текущая страница */
    current_page: 1,

	/** Количество на одной странице */
	page_limit: 5,

	/** Количество записей */
	items_length: 0,

	/** Строка для поиска совпадений в заголовках */
	search_query: '',
  },
  mutations: {
	/** Меняем страницу в пагинации */
    fGoPage(state, data) {
      	state.current_page = data;
    },

	/** Записываем посты с пагинацей */
	fSetPosts(state) {
		const { result, total } = fPaginate(this.getters.nOffset, state.page_limit, state.post_list_full);
		state.post_list = result;
		state.items_length = total;
	},

	/** Поиск по совпадению в заголовках */
	fSearchByQuery(state, data) {
		state.current_page = 1;
		state.search_query = data;

		if (Object.values(this.getters.vSearchQuery).length) {
			const { result, total } = ctrl.fSearchByParam(this.getters.vSearchQuery, this.getters.nOffset, state.page_limit, state.post_list_full);
			state.post_list = result;
			state.items_length = total;
		}
	},

	/** Поиск по интервалу дат */
	fSetDateInterval(state, data) {
		state.date_interval = data;
		state.current_page = 1;
		const { result, total } = ctrl.fSearchByParam(this.getters.vSearchQuery, this.getters.nOffset, state.page_limit, state.post_list_full);
		state.post_list = result;
		state.items_length = total;
	},

	/** Cбросить фильтры и поисковую строку */
	fResetFilters(state) {
		state.search_query = '';
		state.date_interval = [];
		const { result, total } = ctrl.fSearchByParam(this.getters.vSearchQuery, this.getters.nOffset, state.page_limit, state.post_list_full);
		state.post_list = result;
		state.items_length = total;
	},

	/** Пишем полный список постов в state (проводим по нему поиск)  */
	fGetPosts(state, data) {
		state.post_list_full = data;
	},

	/** Пишем индесированный список постов в state  */
	fSetIxPostNews(state, data) {
		state.ix_post_list_full = data;
	},
  },
  actions: {
	/** Получаем посты */
	async faGetPosts({ commit, dispatch }) {
		const avPosts = await ctrl.faGetXMLData();
		const ixPosts = fGetIx(avPosts, 'rbc_news:news_id');
		commit('fSetIxPostNews', ixPosts);
		commit('fGetPosts', avPosts);
		await dispatch('faSetPosts');
	},

	/** Получаем посты с пагинацией */
	async faSetPosts({ commit }) {
		commit('fSetPosts');
	}
  },
  getters: {
	/** Количество записей, которые нужно пропустить */
    nOffset (state) {
      return state.page_limit * state.current_page - state.page_limit;
    },

	/** Количество страниц */
	nPagesCount(state) {
		let nPagesCount = 0;
		const nDivision = state.items_length % state.page_limit;

		if (state.items_length < state.page_limit) {
			nPagesCount = 1;
			return nPagesCount;
		}

		if (nDivision > 0) {
			nPagesCount = Math.ceil((state.items_length / state.page_limit));
			return nPagesCount;
		}
		
		nPagesCount = (state.items_length / state.page_limit);

		return nPagesCount;
	},
	
	/** Объект с параметрами для поиска  */
	vSearchQuery (state) {
		const vSearchQuery = {
			search_query: state.search_query ? state.search_query : '',
			date_interval: state.date_interval ? state.date_interval : [],
		}

		return vSearchQuery;
	}
  }
})

export default store;
