import { routes } from "./routes";
import { XMLParser } from 'fast-xml-parser';
import { fFormatApiDateToISO, fGetMsByDate, fPaginate, fFormatString } from './service/FormatS'

class PostsCtrl {
	/** Получаем данные xml */
	async faGetXMLData() {
		const res = await fetch(`${routes.baseUrl}${routes.getXML}`);
		const reader = await res.body.getReader();
		const data = await reader.read();
		const sDecoded = new TextDecoder().decode(data.value);
		const parser = new XMLParser(sDecoded);
		const vXMlNews = parser.parse(sDecoded);
		const avPosts = vXMlNews.rss.channel.item;

		return avPosts;	
	}

	/** Поиск по совпадению в заголовках, c сохранением пагинации для  результатов*/
	fSearchByParam(vQuery, nOffset, nLimit, avData) {
		const acSearchable = [...avData];
		const aSearchResults = [];

		for (let i = 0; i < acSearchable.length; i++) {
			const vParams = { 
				search_query: vQuery.search_query,
				date_from: vQuery.date_interval[0],
				date_to: vQuery.date_interval[1],
				item: acSearchable[i]
			}

			const { isQueryStringUnderCondition, isDateFromUnderCondition, isDateToUnderCondition } = this.fCheckValueExist(vParams);

			if (isQueryStringUnderCondition && isDateFromUnderCondition && isDateToUnderCondition) {
				aSearchResults.push(acSearchable[i]);
			}
		}

		const vPaginatedData = fPaginate(nOffset, nLimit, aSearchResults);

		return vPaginatedData;
	}

	/** Проверим значения объекта на существование */
	fCheckValueExist(params) {
		const sPostTitle = fFormatString(params.item.title, /[\s.,%,-]/g, '');
		const sSearchQueryClear = fFormatString(params.search_query, /[\s.,%,-]/g, '');
		let isQueryStringUnderCondition = false;
		let isDateFromUnderCondition = false;
		let isDateToUnderCondition = false;

		const newsTimeStampFormatted = fFormatApiDateToISO(params.item["rbc_news:date"]);
		const nItemTimeStamp = fGetMsByDate(newsTimeStampFormatted);
		const date_from = fGetMsByDate(params.date_from);
		const date_to = fGetMsByDate(params.date_to);

		if (sSearchQueryClear.length) {
			isQueryStringUnderCondition = sPostTitle.indexOf(sSearchQueryClear) > -1;
		} else {
			isQueryStringUnderCondition = true;
		}
		
		if (date_from) {
			isDateFromUnderCondition = date_from <= nItemTimeStamp;
		} else {
			isDateFromUnderCondition = true;
		}

		if (date_to) {
			isDateToUnderCondition = date_to >= nItemTimeStamp;
		} else {
			isDateToUnderCondition = true;
		}

		return { isQueryStringUnderCondition, isDateFromUnderCondition, isDateToUnderCondition };
	}
}

export const gPostsCtrl = new PostsCtrl();
