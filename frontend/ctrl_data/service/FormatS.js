import dayjs from 'dayjs';

/** 
 * Форматируем дату поста 
 * @item - String
 * @returns - String
 */
export function fFormatApiDateToISO(item) {
	return item.split('.').reverse().join('-');
}

/** 
 * Переводим дату в милллисекунды 
 * @item - String
 * @returns - Number
 */
export function fGetMsByDate(item) {
	let sDateMs  = '';

	if (item) sDateMs = dayjs(item).unix()

	return sDateMs;
}

/** 
 * Получаем индексированный список
 * @item - Array
 * @key - String
 * @returns - Object
 */
export function fGetIx(item, key) {
	const ixPosts = {};

	for (let i = 0; i < item.length; i++) {
		const iterator = item[i][key];
		ixPosts[iterator] = item[i];
	}

	return ixPosts;
}

/** 
 * Создаём пагинацию,
 * @nOffset - Number
 * @nLimit - Number
 * @aData - any Array
 * @returns - Array
 */
export function fPaginate(nOffset, nLimit, aData) {
	const acData = [...aData];

	return {
		total: acData.length,
		result: acData.splice(nOffset, nLimit)
	}
}

/** 
 * Форматируем строку, заменяя символы
 * @item - String
 * @reg - RegEx
 * @replacer - String
 * @returns - String
 */
export function fFormatString(item, reg, replacer) {
	return item.toLowerCase().replace(reg, replacer);
}
