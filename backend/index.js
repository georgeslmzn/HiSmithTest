const express = require('express');
const fs = require('fs');
const app = express();
const cors = require('cors');

app.use(cors());

app.get('/get-xml', (req, res) => {
	fs.readFile(__dirname + '/data/News.xml', (err, data) => { 
		res.send(data)
	});
})

app.listen(3001);
